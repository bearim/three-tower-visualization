import {BoxBufferGeometry, Group, Mesh, MeshBasicMaterial, MeshLambertMaterial, Vector3} from "three";
import {MountKit, Size} from "../common/Helpers";
import {degreeToRadian, getPointOnCircle} from "../common/Utils";

export const createSquareMount = (mountInfo, level, sideSize, numberOfSlots, includeAngles) => {
    const color = 0xaa33aa;
    const mountModel = createSquareCustomMountBasedOnCircle(4, sideSize, level, color);
    return new MountKit(level, 0, mountInfo, {}, mountModel);
};

export const createSquareCustomMountBasedOnCircle = (sides, size, level, color) => {
    const additionalSpace = size * 0.1;
    const radius = size / (2 * Math.sqrt(3)) + additionalSpace;

    const group = new Group();
    const azimuthStep = 360 / sides;
    let azimuthCurrent = 0;
    while (azimuthCurrent < 360) {
        const point = getPointOnCircle(radius, degreeToRadian(azimuthCurrent), level);
        group.add(createSquareLegForAngle(point, radius, azimuthCurrent, sides, color));
        azimuthCurrent += azimuthStep;
    }
    return group;
};

const createSquareLegForAngle = (point, radius, angle, sides, color) => {
    const side = .2;
    const size = new Size(side, side, 3);
    const position = new Vector3(point.x / 2, point.y, point.z / 2);
    return createBoxMeshAt(position, size, angle, sides, color);
}

const createBoxMeshAt = (point, size, angle, sides, color) => {
    const geometry = new BoxBufferGeometry(size.width, size.height, size.depth, 1);
    const material = new MeshLambertMaterial({color: color});
    const box = new Mesh(geometry, material);
    box.position.set(point.x, point.y, point.z);

    let rotationAddition = 0;
    if (angle / 90 % 2 === 1) {
        rotationAddition = 90;
    }

    box.rotation.y = degreeToRadian(angle * 2) + degreeToRadian(90 + rotationAddition);
    return box;
};