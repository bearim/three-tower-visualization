import {BoxBufferGeometry, Group, Mesh, MeshLambertMaterial, Vector3} from "three";
import {degreeToRadian, getPointOnCircle} from "../common/Utils";
import {MountKit, Size, Slot, SlotDto} from "../common/Helpers";
import {createSquareMount} from "./SquareMount";

const SLOT_INDEXES = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'D', 'E', 'F', 'G']; // add more if needed

export const createMountKit = (level, mountInfo) => {
    const mountType = 'TRIANGLE'; // todo: define this choose
    switch (mountType) {
        case 'TRIANGLE':
            return createTriangleMount(mountInfo, level, 5, 9, false);
        case 'SQUARE': // todo
            return createSquareMount(mountInfo, level, 1, false);
        default : // todo
            return createTriangleMount(mountInfo, level, 5, 6, false);
    }
};

const createTriangleMount = (mountInfo, level, size, numberOfSlots, includeAngles) => {
    const color = 0x7089FF;
    const additionalSpace = size * 0.1;
    const radius = size / (2 * Math.sqrt(3)) + additionalSpace;
    const sides = 3;
    const mountDto = createCustomMountBasedOnCircle(radius, sides, size, level, Math.trunc(numberOfSlots / sides), color);
    return new MountKit(level, 0, mountInfo, mountDto.slots, mountDto.model);
};

const createCustomMountBasedOnCircle = (radius, sides, size, level, slotsNum, color) => {
    const group = new Group();
    const azimuthStep = 360 / sides;
    let azimuthCurrent = 0;
    let slotPointer = 0;
    const slots = [];
    while (azimuthCurrent < 360) {
        const currentRadians = degreeToRadian(azimuthCurrent);
        const point = getPointOnCircle(radius, currentRadians, level);
        group.add(createPanelWithCenterAt(point, size, currentRadians, color));
        group.add(createLegForAngle(point, radius, currentRadians, color));
        createSlotVerticals(radius, currentRadians, size, level, slotsNum, slotPointer, false, color).forEach(slotDto => {
            group.add(slotDto.model);
            slots.push(slotDto.slot);
            slotPointer++;
        });
        azimuthCurrent += azimuthStep;
    }
    group.children.forEach(mesh => {
        mesh.castShadow = true;
        mesh.receiveShadow = true;
    });
    return {
        model: group,
        slots
    };
};

const createSlotVerticals = (radius, angle, sideSize, level, numOfSlots, slotPointer, includeAngles, color) => {
    const slots = [];
    const cat = sideSize / 2; // dont want investigate these coefficients anymore, so it will be hardcoded
    const hyp = Math.sqrt(Math.pow(radius - sideSize * .06, 2) + Math.pow(cat, 2));
    const radians = cat / hyp;
    const position1 = getPointOnCircle(hyp, -radians + angle, level);
    const position2 = getPointOnCircle(hyp, radians + angle, level);
    const position3 = getPointOnCircle(radius + .1, angle, level);
    const size = new Size(.2, 3, .2);
    const slot1 = createBoxMeshAt(position1, size, angle, color);
    const slot2 = createBoxMeshAt(position2, size, angle, color);
    const slot3 = createBoxMeshAt(position3, size, angle, color);
    slots.push(new SlotDto(new Slot(SLOT_INDEXES[slotPointer + 0], position1, radians), slot1));
    slots.push(new SlotDto(new Slot(SLOT_INDEXES[slotPointer + 1], position2, radians), slot2));
    slots.push(new SlotDto(new Slot(SLOT_INDEXES[slotPointer + 2], position3, radians), slot3));
    return slots;
};

const createLegForAngle = (point, length, angle, color) => {
    const side = .2;
    const size = new Size(side, side, length);
    const position = new Vector3(point.x / 2, point.y, point.z / 2);
    return createBoxMeshAt(position, size, angle, color);
};

const createPanelWithCenterAt = (point, longSide, angle, color) => {
    const size = new Size(longSide, .2, .1);
    return createBoxMeshAt(point, size, angle, color);
};

const createBoxMeshAt = (point, size, angle, color) => {
    const geometry = new BoxBufferGeometry(size.width, size.height, size.depth, 1);
    const material = new MeshLambertMaterial({color: color});
    const box = new Mesh(geometry, material);
    box.position.set(point.x, point.y, point.z);
    box.rotation.y = angle * 2 + degreeToRadian(90);
    return box;
};
