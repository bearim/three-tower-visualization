import {Vector3} from "three";

export const degreeToRadian = degree => degree * (Math.PI / 180);

export const convertInchToFeet = inch => inch / 12;

export const getPointOnCircle = (radius, angle, level) => {
    return new Vector3(radius * Math.cos(angle), level, radius * Math.sin(angle));
};
