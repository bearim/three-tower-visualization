export class Size {
    height;
    width;
    depth;

    constructor(width, height, depth) {
        this.width = width;
        this.height = height;
        this.depth = depth;
    }
}

export class MountKit {
    level;
    azimuth; // rotation of mountInfo level
    slots; // slots map: id, position
    installed; // map of installed devices: slotId, device
    mountInfo; // original object which describe level
    model; // three js model

    constructor(level, azimuth, mountInfo, slots, model) {
        this.level = level;
        this.azimuth = azimuth;
        this.mountInfo = mountInfo;
        this.slots = slots;
        this.installed = {};
        this.model = model;
    }
}

export class Slot {
    id;
    point;
    angle;

    constructor(id, point, angle) {
        this.id = id;
        this.point = point;
        this.angle = angle;
    }
}

export class SlotDto {
    slot;
    model;

    constructor(slot, model) {
        this.slot = slot;
        this.model = model;
    }
}

export class Device {
    azimuth;
    device;
    model;

    constructor(azimuth, device, model) {
        this.azimuth = azimuth;
        this.device = device;
        this.model = model;
    }
}
