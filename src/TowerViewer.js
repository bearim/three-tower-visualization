import React, {Component} from "react";
import {
    AmbientLight,
    BoxGeometry,
    Color,
    CylinderGeometry,
    DirectionalLight,
    Fog,
    Mesh,
    MeshLambertMaterial,
    MeshPhongMaterial,
    ObjectLoader,
    PerspectiveCamera,
    PlaneBufferGeometry,
    RepeatWrapping,
    Scene,
    TextureLoader,
    WebGLRenderer,
    ConeGeometry,
    MeshBasicMaterial,
    Vector3
} from "three";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
// import {getTowerLevelsWithEquipment} from "../../services/towersAPI";
import DatGui, {DatBoolean} from "react-dat-gui";
import {createMountKit} from "./mounts/Mount";
import {Device} from "./common/Helpers";
import {convertInchToFeet, degreeToRadian} from "./common/Utils";
import {getMockData} from "./common/MockDataToBeRemovedLater";
import {DragControls} from "three/examples/jsm/controls/DragControls";
import ground from "./textures/grasslight-big.jpg";
import {LineGeometry} from "three/examples/jsm/lines/LineGeometry";

export const ViewerConfiguration = {
    fieldOfView: 45,
    nearestPoint: 0.1,
    furthestPoint: 10000
};

class TowerViewer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isShow: true,
            levels: [],
            tubesSlots: [],
            controlPanel: {
                isValidateOccupied: false,
                isCheckAvailability: false
            }
        };
    }

    loader = new ObjectLoader();
    mount;
    renderer;
    scene;
    camera;
    orbitControls;
    dragControls;
    ground;
    compass = {
        cylinder: null,
        northArrow: null,
        southArrow: null
    };
    redArrow;
    whiteArrow;
    current = {
        tower: null,
        towerHeight: 0,
        mountModels: [], // mounts list
        deviceModels: [], // devices list
        levelTubes: [], // level tubes list
        mount: {}, // map: level id, mountInfo obj
        slots: []
    };

    getTowerHeight = () => {
        return this.current.towerHeight;
    };

    componentDidMount() {
        this.configureScene();

        // entry point for data rendering
        // if (this.props.towerDetails) {
        //     getTowerLevelsWithEquipment(this.props.towerDetails.id).then(({data: levelsWithEquipment}) => {
        //         this.setState({
        //             levels: levelsWithEquipment
        //         })
        //         this.current.towerHeight = 180;
        //         this.addExampleComponents(levelsWithEquipment);
        //     });
        // } else {
            const data = getMockData();
            this.current.towerHeight = 180;
            this.addExampleComponents(data);
        // }
    }

    configureScene = () => {
        this.renderer = new WebGLRenderer();
        this.scene = new Scene();
        this.camera = new PerspectiveCamera(ViewerConfiguration.fieldOfView,
            this.mount.clientWidth / this.mount.clientHeight,
            ViewerConfiguration.nearestPoint,
            ViewerConfiguration.furthestPoint);
        this.orbitControls = new OrbitControls(this.camera, this.renderer.domElement);
        this.dragControls = new DragControls(this.current.deviceModels, this.camera, this.renderer.domElement);

        this.orbitControls.addEventListener( 'change', () => {this.configureCompass()});

        this.dragControls.addEventListener( 'dragstart', () => { this.orbitControls.enabled = false });
        this.dragControls.addEventListener( 'drag', event => { this.fixateDetailsOnTower(event) });
        this.dragControls.addEventListener( 'dragend', () => { this.orbitControls.enabled = true; });

        this.renderer.setSize(this.mount.clientWidth, this.mount.clientHeight);
        this.renderer.shadowMap.enabled = true;

        this.scene.background = new Color(0xcce0ff);
        this.scene.fog = new Fog(0xcce0ff, 250, 5000);
        this.camera.position.z = 100; // default position
        this.camera.lookAt(0, 0, 0);

        this.compass = this.initializeCompass();
        this.initializeNorthArrow();
        this.initializeSouthArrow();
        this.scene.add(this.compass);
        this.scene.add(this.whiteArrow);
        this.scene.add(this.redArrow);
        this.mount.appendChild(this.renderer.domElement);

        this.configureLights();
        this.configureGround();

        this.animate();
    };

    initializeCompass = () => {
        const cylinderGeometry = new CylinderGeometry( 1, 1, 0.25, 32 );
        const cylinderMaterial = new MeshBasicMaterial( {color: 0x2a9df4} );
        const cylinder = new Mesh(cylinderGeometry, cylinderMaterial);

        return cylinder;
    };

    initializeSouthArrow = () => {
        const firstConeGeometry = new ConeGeometry( 0.5, 0.25, 4 );
        const firstConeMaterial = new MeshBasicMaterial( {color: 0xffffff } );
        const firstCone = new Mesh(firstConeGeometry, firstConeMaterial);
        firstCone.scale.set(0.5, 1, 2);

        this.redArrow = firstCone;
    };

    initializeNorthArrow = () => {
        const secondConeGeometry = new ConeGeometry( 0.5, 0.25, 4 );
        const secondConeMaterial = new MeshBasicMaterial( {color: 0xed2939 } );
        const secondCone = new Mesh(secondConeGeometry, secondConeMaterial);
        secondCone.scale.set(0.5, 1, 2);

        this.whiteArrow = secondCone;
    };

    configureCompass = () => {
        const vec = new Vector3(0, 0, 0) ;
        const rot = new Vector3(0, 0, 0);

        this.compass.position.copy( this.camera.position );
        this.compass.rotation.copy( this.camera.rotation );
        this.compass.updateMatrix();
        this.compass.translateZ( - 10 );
        this.compass.translateX(-3);
        this.compass.translateY( -2);
        this.compass.rotation.setFromVector3(vec);

        this.whiteArrow.position.x = this.camera.position.x;
        this.whiteArrow.position.y = this.camera.position.y + 0.25;
        this.whiteArrow.position.z = this.camera.position.z - 0.01;

        this.whiteArrow.rotation.copy( this.camera.rotation );

        this.whiteArrow.updateMatrix();
        this.whiteArrow.translateZ( - 10 );
        this.whiteArrow.translateX(-3);
        this.whiteArrow.translateY( -2);
        this.whiteArrow.rotation.setFromVector3(rot);

        this.redArrow.position.x = this.camera.position.x;
        this.redArrow.position.y = this.camera.position.y + 0.25;
        this.redArrow.position.z = this.camera.position.z + 0.01;

        this.redArrow.rotation.copy( this.camera.rotation );

        this.redArrow.updateMatrix();
        this.redArrow.translateZ( - 10 );
        this.redArrow.translateX(-3);
        this.redArrow.translateY( -2);
        this.redArrow.rotation.setFromVector3(rot);
    };


    configureLights = () => {
        const light = new AmbientLight(0xffffff, .7);
        this.scene.add(light);
        const additionalLight = new DirectionalLight(0xffffff);
        additionalLight.position.set(100, 200, 100);
        additionalLight.position.multiplyScalar(1.3);
        additionalLight.castShadow = true;
        additionalLight.shadow.mapSize.width = 8096;
        additionalLight.shadow.mapSize.height = 8096;
        const d = 300;
        additionalLight.shadow.camera.left = -d;
        additionalLight.shadow.camera.right = d;
        additionalLight.shadow.camera.top = d;
        additionalLight.shadow.camera.bottom = -d;
        additionalLight.shadow.camera.far = 1000;
        this.scene.add(additionalLight);
    };

    configureGround = () => {
        new TextureLoader().load(ground, groundTexture => {
            groundTexture.wrapS = RepeatWrapping;
            groundTexture.wrapT = RepeatWrapping;
            groundTexture.repeat.set(200, 200);
            groundTexture.anisotropy = 16;

            const groundMaterial = new MeshLambertMaterial({map: groundTexture});

            this.ground = new Mesh(new PlaneBufferGeometry(20000, 20000), groundMaterial);
            const towerHeight = this.getTowerHeight();
            this.ground.position.y = towerHeight ? -towerHeight / 2 : -200;
            this.ground.rotation.x = -Math.PI / 2;
            this.ground.receiveShadow = true;
            this.scene.add(this.ground);
        });
    };

    animate = () => {
        requestAnimationFrame(this.animate);
        this.orbitControls.update();
        this.renderer.render(this.scene, this.camera);
    };

    setTower = tower => {
        if (this.current.tower) {
            this.scene.remove(this.current.tower);
        }

        this.current.tower = tower;
        tower.castShadow = true;
        tower.receiveShadow = true;
        this.scene.add(tower);
        const height = this.getTowerHeight();
        this.camera.position.z = height * 1.5;
        if (this.ground) {
            this.ground.position.y = -height / 2
        }
    };

    fixateDetailsOnTower = event => {
        let fixedPoint = this.calculateClosestPoint(event.object.position);

        event.object.position.x = fixedPoint.x;
        event.object.position.z = fixedPoint.z;
        event.object.position.y = fixedPoint.y;

        event.object.setRotationFromEuler(this.calculateClosestMesh(fixedPoint).rotation);
    };

    calculateClosestMesh = slotPoint => {
        let minimumDistance = 100000;
        let meshTo;
        this.current.mountModels.forEach( group  => {
            group.children.forEach( mesh => {
                let distance = mesh.position.distanceTo(slotPoint);
                if (minimumDistance > distance) {
                    minimumDistance = distance;
                    meshTo = mesh;
                }
            })
        })
        return meshTo;
    }

    calculateClosestPoint = pointFrom => {
        let min = 100000;
        let pointTo;
        this.current.slots.forEach( vector => {
            let distance = vector.distanceTo(pointFrom);
            if (min > distance) {
                min = distance;
                pointTo = vector;
            }
        })
        return pointTo;
    }

    addMount = (mountKit) => {
        this.current.mountModels.push(mountKit.model);
        this.current.mount[mountKit.level] = mountKit;
        this.scene.add(mountKit.model);
        mountKit.slots.forEach( slot => {
            this.current.slots.push(slot.point)
        });
    };

    removeMount = levelNum => {
        const mountKit = this.current.mount[levelNum];
        this.scene.remove(mountKit.model);
        Object.keys(mountKit.installed).forEach(slot => this.removeDevice(levelNum, slot));
        this.current.mountModels.splice(this.current.deviceModels.indexOf(mountKit.model), 1);
    };

    addDevice = (levelNum, slotId, device) => {
        const mountKit = this.current.mount[levelNum];
        if (!mountKit) return; // todo: do something with this
        if (mountKit.installed[slotId]) return;//throw new Error('Specified slot already have installed device');
        mountKit.installed[slotId] = device;
        device.model.castShadow = true;
        device.model.receiveShadow = true;
        this.current.deviceModels.push(device.model);
        this.scene.add(device.model);
    };

    removeDevice = (level, slot) => {
        const kit = this.current.mount[level];
        if (!kit) return;
        const device = kit.installed[slot];
        if (!device) return;
        this.scene.remove(device.model);
        this.current.deviceModels.splice(this.current.deviceModels.indexOf(device.model), 1);
        delete kit.installed[slot];
    };

    addLevelTube = tube => {
        this.current.levelTubes.push(tube);
        this.scene.add(tube)
    };

    removeLevelTube = tube => {
        this.scene.remove(tube);
        this.current.levelTubes.splice(this.current.levelTubes.indexOf(tube), 1);
    };

    handleControlPanelUpdate = newControlPanelState => {
        this.setState({
            controlPanel: newControlPanelState
        })
        if (newControlPanelState.isValidateOccupied) {
            this.state.levels.forEach(level => {
                const device = this.createExampleTube(this.getTowerHeight(), level);
                const slotId = Math.random() * 10;
                this.setState(state => ({
                    tubesSlots: [...state.tubesSlots, {slotId, level}]
                }))
                this.addDevice(level.mountLevel - this.getTowerHeight() / 2, slotId, device);
            });
        } else {
            this.state.levels.forEach(level => {
                this.state.tubesSlots.forEach(slot => {
                    if (slot.level === level) {
                        this.removeDevice(level.mountLevel - this.getTowerHeight() / 2, slot.slotId)
                    }
                })
                this.setState({
                    tubesSlots: []
                })
            })
        }
    }

    render() {
        return (
            <div style={{width: '800px', height: '800px'}} ref={ref => (this.mount = ref)}>
                <DatGui style={{position: 'absolute'}} data={this.state.controlPanel}
                        onUpdate={this.handleControlPanelUpdate}>
                    <DatBoolean path={'isValidateOccupied'} label={'Validate occupied'}/>
                    <DatBoolean path={'isCheckAvailability'} label={'Check availability'}/>
                </DatGui>
            </div>
        )
    }

    /**
     * Following code should be removed before the release, as it only example methods and components.
     */

    loadModelExampleDoNotUse = (path, callback, progress, error) => {
        this.loader.load("models/json/example.json",
            obj => this.scene.add(obj),
            xhr => console.log((xhr.loaded / xhr.total * 100) + '% loaded'),
            err => console.error('An error happened: ', err)
        )
    };

    createExampleTower = height => {
        const radialSegmentsNumber = height * 100;
        const widthTop = this.getTowerWidthAtTop(height);
        const widthBottom = this.getTowerWidthByLevelFromTop(height, 0);
        const cylinderGeometry = new CylinderGeometry(widthTop, widthBottom, height, radialSegmentsNumber);
        const cylinderMaterial = new MeshLambertMaterial({color: 0x999999});
        return new Mesh(cylinderGeometry, cylinderMaterial);
    };

    getTowerWidthAtTop = height => height / 1000;
    getTowerWidthByLevelFromTop = (height, level) => {
        const topWidth = this.getTowerWidthAtTop(height);
        return (5 - ((level / height) * 5)) * topWidth + topWidth;
    };

    createExampleLevelTube = (height, levelInfo) => {
        const level = levelInfo.mountLevel;
        const blockHeight = height / 100;
        const halfBlockHeight = blockHeight / 2;
        const widthTop = this.getTowerWidthByLevelFromTop(height, level + halfBlockHeight) * 1.02;
        const widthBottom = this.getTowerWidthByLevelFromTop(height, level - halfBlockHeight) * 1.02;
        const color = levelInfo.status === 'AVAILABLE' ? 0x6FF496 : 0x7089FF;
        const radialSegmentsNumber = 100;
        const cylinderGeometry = new CylinderGeometry(widthTop, widthBottom, blockHeight, radialSegmentsNumber);
        const cylinderMaterial = new MeshLambertMaterial({color: color});
        const mesh = new Mesh(cylinderGeometry, cylinderMaterial);
        mesh.castShadow = true;
        mesh.receiveShadow = true;
        mesh.position.set(0, level - (height / 2), 0);
        return mesh;
    };

    createExampleTube = (height, levelInfo) => {
        const isLevelAvailable = levelInfo.status === 'AVAILABLE';
        const maxAntennaLength = () => {
            return Math.max(...levelInfo.antennas.map(antenna => convertInchToFeet(antenna.equipment.height)))
        }
        const level = levelInfo.mountLevel;
        const blockHeight = isLevelAvailable ? 4 : maxAntennaLength();
        const width = 7;
        const color = isLevelAvailable ? 0x00aa00 : 0x6666ff;
        const radialSegmentsNumber = 100;
        const cylinderGeometry = new CylinderGeometry(width, width, blockHeight, radialSegmentsNumber);
        const cylinderMaterial = new MeshPhongMaterial({color: color, opacity: .2, transparent: true});
        const mesh = new Mesh(cylinderGeometry, cylinderMaterial);
        mesh.position.set(0, level - (height / 2), 0);
        return new Device(0, levelInfo, mesh);
    };

    createExampleDevice = (height, deviceInfo) => {
        const levelNum = deviceInfo.mountLevel - height / 2;
        const slotId = deviceInfo.antennaPosition;
        const azimuth = degreeToRadian(deviceInfo.azimuthDeg);
        const mountKit = this.current.mount[levelNum];
        let device;
        if (mountKit && mountKit.slots[slotId]) { // new code
            const slot = mountKit.slots[slotId];

            const geometry = new BoxGeometry(
                convertInchToFeet(deviceInfo.equipment.width),
                convertInchToFeet(deviceInfo.equipment.height),
                convertInchToFeet(deviceInfo.equipment.depth),
                1);
            const material = new MeshLambertMaterial({color: this.getRandomColor()});
            const mesh = new Mesh(geometry, material);
            mesh.position.set(slot.point.x, slot.point.y, slot.point.z);
            mesh.rotateY(slot.angle + azimuth);

            device = new Device(azimuth, deviceInfo, mesh);
            this.addDevice(levelNum, slotId, device);
        } else {
            // todo: remove this when data is fixed
            // devices which not matched to any mount kit or slots on it, dont know what to do with this
            const radius = this.getTowerWidthByLevelFromTop(height, deviceInfo.mountLevel) * 1.01 + convertInchToFeet(deviceInfo.equipment.depth) / 2;
            const radialSegmentsNumber = 1;
            const geometry = new BoxGeometry(
                convertInchToFeet(deviceInfo.equipment.width),
                convertInchToFeet(deviceInfo.equipment.height),
                convertInchToFeet(deviceInfo.equipment.depth), radialSegmentsNumber);
            const boxMaterial = new MeshLambertMaterial({color: this.getRandomColor()});
            const mesh = new Mesh(geometry, boxMaterial);
            mesh.position.set(radius * Math.cos(azimuth), levelNum, radius * Math.sin(azimuth));
            mesh.rotateY(azimuth + degreeToRadian(90));
            device = new Device(0, deviceInfo, mesh);
            this.addDevice(levelNum, Math.random(), device);
        }
    };

        addExampleComponents = (data) => {
        // cleanup
        this.current.deviceModels.forEach(this.scene.remove);

        // add new
        const towerHeight = this.getTowerHeight();
        this.setTower(this.createExampleTower(towerHeight));
        data.forEach(level => {
            const devices = level.antennas;
            const levelInfo = level.towerLevel;
            if (levelInfo.status !== 'AVAILABLE') {
                const mountKit = createMountKit(levelInfo.mountLevel - towerHeight / 2, levelInfo);
                this.addMount(mountKit);
            }
            devices.forEach(device => this.createExampleDevice(towerHeight, device));

            // todo: refactor this
            const levelTube = this.createExampleLevelTube(towerHeight, levelInfo);
            this.addLevelTube(levelTube);
        });
    };

    getRandomColor = () => {
        const letters = '0123456789ABCDEF';
        let color = '#';
        for (let i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    };
}

export default TowerViewer;
